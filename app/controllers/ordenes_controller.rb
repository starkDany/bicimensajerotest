class OrdenesController < ApplicationController

 def new
 	@orden = Orden.new
 end
 
  def create
    @orden = current_user.ordenes.build(orden_params)
    if @orden.save
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
      render 'static_pages/home'
    end
  end

  def destroy
  end

  private

    def orden_params
      params.require(:orden).permit(:content)
    end
end