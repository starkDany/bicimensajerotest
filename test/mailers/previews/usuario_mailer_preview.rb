# Preview all emails at http://localhost:3000/rails/mailers/usuario_mailer
class UsuarioMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/usuario_mailer/account_activation
  def account_activation
    usuario = Usuario.first
    usuario.activation_token = Usuario.new_token
    UsuarioMailer.account_activation(usuario)
  end

  # Preview this email at http://localhost:3000/rails/mailers/usuario_mailer/password_reset
  def password_reset
    usuario = Usuario.first
    usuario.reset_token = Usuario.new_token
    UsuarioMailer.password_reset(usuario)
  end

end
